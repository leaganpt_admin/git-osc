---
layout: post
title: "码云新增多处宽屏功能，优化细节体验"
---

经常使用码云的用户都非常了解，码云的版式是居中定宽的，看代码Diff的时候如果代码写的比较长，就需要横向拖动，体验非常不好，再比如改版后的项目 Wiki ，虽然操作上体验提升了很多，但是由于是左右两栏，导致可编辑的区域就变小了，我们这次着重优化了代码Diff、Wiki、新增代码片段等功能界面，使之支持全屏模式，在查看和编辑的时候区域更大。

### Diff 宽屏模式

![![输入图片说明](https://gitee.com/uploads/images/2017/1019/182933_24371427_62561.png "屏幕截图.png")](https://gitee.com/uploads/images/2017/1019/182932_1604135e_62561.png "屏幕截图.png")

PR Diff 也增加了宽屏模式，方便审核查看。

![输入图片说明](https://gitee.com/uploads/images/2017/1019/183545_605e3965_62561.png "屏幕截图.png")

### 项目 Wiki宽屏模式

![输入图片说明](https://gitee.com/uploads/images/2017/1019/183942_a5023d21_62561.png "屏幕截图.png")

### 发布代码片段全屏模式

![输入图片说明](https://gitee.com/uploads/images/2017/1019/184109_197bab59_62561.png "屏幕截图.png")

赶快来试试吧 -> https://gitee.com

