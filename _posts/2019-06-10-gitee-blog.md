---
layout: post
title: "坑爹代码 | 你是如何被异常玩然后变成玩异常的？"
---

<p>玩 Java 的人，刚开始会被各种异常虐，空指针应该是最常见的。多玩两年就开始玩异常，各种奇葩异常玩法层出不穷。</p>

<p>你觉得下面这种异常的定义妥吗？</p>

<pre>
<code class="language-java">public class CommandException extends BaseException {

	private static final long serialVersionUID = -6354513454371927970L;
	
	public static CommandException PARAM_NULL= new CommandException("Command_assemble_01", "Parameter is Needed But Empty");
	public static CommandException DEVID_NULL = new CommandException("Command_assemble_02", "DevId Cannot Be Null");
	public static CommandException MDCODE_NULL = new CommandException("Command_assemble_03", "Model Code Cannot Be Empty");
	public static CommandException ORDER_NULL = new CommandException("Command_assemble_04", "Order Cannot Be Empty");
	public static CommandException TYPE_NULL = new CommandException("Command_assemble_05", "Upstream / Downstream Type Cannot Be Empty");
	public static CommandException MENUID_NULL = new CommandException("Command_assemble_06", "Menu Id Cannot Be Null");
	public static CommandException CTRLTYPE_NOT_RANGE= new CommandException("Command_assemble_07", "Ctrltype Cannot Be Recognized, Which is not in Range");
	public static CommandException CMD_NULL = new CommandException("Command_analyze_01", "CMD Cannot Be Null");
	public static CommandException PAYLOAD_NULL = new CommandException("Command_analyze_02", "Payload Cannot Be Null");
	public static CommandException FRAMEWORK_FAILED= new CommandException("Command_analyze_03", "Framework Failed to be Checked");
	public static CommandException CHECK_FAILED= new CommandException("Command_analyze_04", "Command Failed to be Checked");
	public static CommandException CONFIGURE_NOT_EXIST = new CommandException("Command_error_1001", "Configure is not Exist");
	public static CommandException REDIS_ERROR = new CommandException("Command_error_1002", "Cache Command in Redis Error", true);
	
	public CommandException() {
		super();
	}</code></pre>

<p>如果不妥，有什么问题呢？&nbsp;</p>

<p>请到下面链接发表评论，领取奖品：</p>

<p><a href="https://gitee.com/oschina/bullshit-codes/blob/master/java/BadException.java">https://gitee.com/oschina/bullshit-codes/blob/master/java/BadException.java</a></p>

<p>码云 6 周年，我们正在征集各种坑爹代码，很多奖品等你来拿</p>

<p>详细的参与方法请看&nbsp;&nbsp;<a href="https://gitee.com/oschina/bullshit-codes">https://gitee.com/oschina/bullshit-codes</a></p>

<p>------ 分割线 ------</p>

<p>其他坑爹代码吐槽：</p>

<ul>
	<li><a href="https://www.oschina.net/news/107032/gitee-bullshit-codes-stringbuffer" target="_blank">坑爹代码&nbsp;| 这样使用 StringBuffer 的方法有什么坑？</a></li>
	<li><a href="https://www.oschina.net/news/107081/gitee-bad-code-one-line-code" target="_blank">坑爹代码&nbsp;| 你写过的最长的一行代码有多长？？？</a></li>
	<li><a href="https://www.oschina.net/news/107151/nesting-bad-code">坑爹代码 | 循环+条件判断，你最多能嵌套几层？</a></li>
	<li><a href="https://www.oschina.net/news/107213/gitee-bullshit-codes-optimize-for-future">坑爹代码 | 为了后期优化查询速度 ~ 颇有商业头脑！</a></li>
</ul>
