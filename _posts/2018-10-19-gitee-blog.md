---
layout: post
title: "CSDN CODE 将于本月底彻底关闭，请尽快迁移到码云"
---

<p>感谢各位小伙伴在过去 5 年里对 CSDN 社区 CODE 平台的支持以及建议。</p><p>为了方便各位小伙伴，迁移出相关的代码：</p><p><span style="color: rgb(255, 0, 0);">&nbsp;&nbsp;&nbsp;我们将于2018年10月31日彻底关闭CODE服务， 迁移过渡时间从今天就可以开始啦。</span></p><p><span style="color: rgb(255, 0, 0);">&nbsp;&nbsp;&nbsp;代码迁移请从git接口，方便大家导出存放在CODE中的完整代码。</span></p><p>对于没有其他代码托管平台账号的用户，建议先将代码克隆到本地（支持SSH和HTTPS），便于以后管理维护。</p><p>码云平台一键迁移，请参考：<a href="https://my.oschina.net/gitosc/blog/1534891">https://my.oschina.net/gitosc/blog/1534891</a></p><p>再次感谢大家对此次社区工作的理解和配合。</p><p><br/>CSDN<br/>2018.9.26</p>